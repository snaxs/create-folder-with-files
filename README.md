You will create a Project and called whatever you want, then you will click on Clone and copy the link of the HTTPS nto the SSH.
You will create a .txt file to paste the URL of your project.
Then you will create a folder named PROJECT or whatever you want, another folder inside called GitLab and another one with your projects name.
Now you will go to CMD and write cd.. 

until you get to C:

When you are on C: you will write cd project\GitLab\YOURPROJECTNAME

click enter and you will see:

C:\project\GitLab\yourname>

Now follow these steps:

Step 1: Download git from https://git-scm.com/ and install git 

Step 2: Check git is installed on your system
   git --version

Step 3: Run following git commands
   git config --global user.name “YOURGITUSERNAME”
   git config --global user.name

   git config --global user.email “YOURGITEMAIL”
   git config --global user.email

   git config --global --list
   
Step 4: Go to cmd 
     CD to the location of the folder and run following commands
   git init
   git status
   git add .
   git commit -m “NAME OF THE PROJECT YOU WANT”
   git push -u “THE URL YOU PASTED ON THE .TXT” master


